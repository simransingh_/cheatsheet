<?php

function db()
{
    $host = 'localhost';
    $database = 'webshop';
    $username = 'root';
    $password = '';

    try {
        $connection = new PDO('mysql:host='.$host.';dbname='.$database, $username, $password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $connection;
    }
    catch(PDOException $e) {
        dd($e->getMessage());
    }
}


//SELECT QUERY
// verbinding maken met database 

$connection = db();

// querie draaien:SELECT * FROM products 
try{
	$products = $connection->prepare('SELECT * FROM products');
	$products->execute([]);
	$products->setFetchMode(PDO::FETCH_ASSOC);
	$products = $products->fetchAll();

}
catch(PDOException $e){
	print_r($e->getMessage());
	die();
}


// data outputten

print_r($products);

// 	INSET Query
//verbinding maken met database

//query draaien 



//UPDATE QUERY
//verbinding maken met database

//QUERY draaien



//DELETE query
//verbinding maken met database

// query draaien